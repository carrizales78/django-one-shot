from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        'lists': lists
    }
    return render(request, 'todos/todo_list_list.html', context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    items = todo_list.items.all()
    context = {
        'todo_list': todo_list,
        'items': items,
    }

    return render(request, 'todos/todo_list_detail.html', context)


def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id=list.id)
    else:
        form = TodoListForm()

    context = {
        'form': form,
    }

    return render(request, 'todos/todo_list_create.html', context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
    return render(request, 'todos/todo_list_update.html', {'form': form})


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        todo_list.delete()
        return redirect('todo_list_list')
    return render(
        request,
        'todos/todo_list_delete.html',
        {'todo_list': todo_list}
        )


def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect('todo_list_detail', id=todo_item.list.id)
    else:
        form = TodoItemForm()
    return render(
        request,
        'todos/todo_item_create.html',
        {'form': form}
        )


def todo_item_update(request, id):
    instance = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=instance.list.id)
    else:
        form = TodoItemForm(instance=instance)
        return render(request, 'todos/todo_item_update.html', {'form': form})
